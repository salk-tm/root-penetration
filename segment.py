import albumentations as album
import argparse
import csv
import cv2
import glob
import json
import math
import numpy as np
import os
import pandas as pd
from PIL import Image
from scipy import stats
import segmentation_models_pytorch as smp
import segmentation_models_pytorch.utils
import sys
import torch
import warnings

os.environ["CUDA_VISIBLE_DEVICES"] = "0,1"
warnings.filterwarnings("ignore")


def crop_save_image(image, bbox, scan_path, frame):
    """Crop image and save the cropped images."""
    (roi_x, roi_y, width, height) = bbox
    new_image = image[roi_y : roi_y + height, roi_x : roi_x + width, :]
    save_path = scan_path + "_crop"
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    cv2.imwrite(os.path.join(save_path, frame), new_image)
    return new_image, save_path


def crop_save_image_plant(scans_df):
    """Crop the images for each plant folder"""
    save_paths = []
    for i in range(len(scans_df)):
        plant_name = scans_df["plant_qr_code"][i]
        # LW [modify the scan path if needed] get the scan_path and add "./data/"
        scan_path = "./data/" + scans_df["scan_path"][i]
        # scanner = scans_df["scanner"][i]
        roi_x, roi_y = scans_df["roi_x"][i], scans_df["roi_y"][i]
        width, height = scans_df["roi_w"][i], scans_df["roi_h"][i]
        bbox = (roi_x, roi_y, width, height)

        # get the images/frames
        frames = [file for file in os.listdir(scan_path) if not file.startswith(".")]

        for frame in frames:
            # crop the original image and save the cropped image
            image_original = cv2.imread(os.path.join(scan_path, frame))
            image, save_path = crop_save_image(image_original, bbox, scan_path, frame)
        save_paths.append(save_path)
    return save_paths


def write_metadata(crop_paths, input_dir):
    # get all images
    image_list = [
        os.path.join(path, file) for path in crop_paths for file in os.listdir(path)
    ]
    # write csv
    metadata_row = []
    for i in range(len(image_list)):
        image_path_i = image_list[i]
        metadata_row.append([str(i + 1), image_path_i, image_path_i])
    # save csv file
    metadata_file = os.path.join(input_dir, "metadata_tem.csv")
    header = ["image_id", "image_path", "label_colored_path"]
    with open(metadata_file, "w") as csvfile:
        writer = csv.writer(csvfile, lineterminator="\n")
        writer.writerow([g for g in header])
        for x in range(len(metadata_row)):
            writer.writerow(metadata_row[x])
    return metadata_file


def setup_model_parameters(input_dir, model_name, DEVICE):
    # load best saved model checkpoint from the current run
    if os.path.exists(f"{model_name}.pth"):
        best_model = torch.load(f"{model_name}.pth", map_location=DEVICE)
        print("Loaded UNet model from this run.")
    else:
        raise ValueError("Model not available!")

    # setup model parameters
    ENCODER = "resnet101"
    ENCODER_WEIGHTS = "imagenet"
    preprocessing_fn = smp.encoders.get_preprocessing_fn(ENCODER, ENCODER_WEIGHTS)

    # check the color
    class_dict = pd.read_csv(os.path.join(input_dir, "label_class_dict_lr.csv"))
    class_names = class_dict["name"].tolist()
    class_rgb_values = class_dict[["r", "g", "b"]].values.tolist()

    select_classes = ["background", "root"]
    select_class_indices = [class_names.index(cls.lower()) for cls in select_classes]
    select_class_rgb_values = np.array(class_rgb_values)[select_class_indices]
    return best_model, select_classes, select_class_rgb_values, preprocessing_fn


class PredictionDataset(torch.utils.data.Dataset):
    """Read images, apply augmentation and preprocessing transformations."""

    def __init__(
        self,
        df,
        class_rgb_values=None,
        augmentation=None,
        preprocessing=None,
    ):
        self.image_paths = df["image_path"].tolist()

        self.class_rgb_values = class_rgb_values
        self.augmentation = augmentation
        self.preprocessing = preprocessing

    def __getitem__(self, i):
        # read images and masks
        image = cv2.cvtColor(cv2.imread(self.image_paths[i]), cv2.COLOR_BGR2RGB)
        names = self.image_paths[i]

        # apply augmentations
        if self.augmentation:
            sample = self.augmentation(image=image)
            image = sample["image"]

        # apply preprocessing
        if self.preprocessing:
            sample = self.preprocessing(image=image)
            image = sample["image"]

        return image, names

    def __len__(self):
        # return length of
        return len(self.image_paths)


def to_tensor(x, **kwargs):
    return x.transpose(2, 0, 1).astype("float32")


def get_preprocessing(preprocessing_fn=None):
    """Construct preprocessing transform
    Args:
        preprocessing_fn: data normalization function
            (can be specific for each pretrained neural network)
    Return:
        transform: albumentations.Compose
    """
    _transform = []
    if preprocessing_fn:
        _transform.append(album.Lambda(image=preprocessing_fn))
    _transform.append(album.Lambda(image=to_tensor, mask=to_tensor))

    return album.Compose(_transform)


def get_validation_augmentation():
    # Add sufficient padding to ensure image is divisible by 32
    test_transform = [
        album.PadIfNeeded(
            min_height=1024, min_width=1024, always_apply=True, border_mode=0
        ),
    ]
    return album.Compose(test_transform)


def setup_dataset(metadata_path, select_class_rgb_values, preprocessing_fn):
    metadata_df = pd.read_csv(metadata_path)
    test_dataset = PredictionDataset(
        metadata_df,
        augmentation=get_validation_augmentation(),
        preprocessing=get_preprocessing(preprocessing_fn),
        class_rgb_values=select_class_rgb_values,
    )
    test_dataset_vis = PredictionDataset(
        metadata_df,
        class_rgb_values=select_class_rgb_values,
    )
    return test_dataset, test_dataset_vis


def crop_image(image, true_dimensions):
    return album.CenterCrop(p=1, height=true_dimensions[0], width=true_dimensions[1])(
        image=image
    )


def colour_code_segmentation(image, label_values):
    """
    Given a 1-channel array of class keys, colour code the segmentation results.

    Args:
        image: single channel array where each value represents the class key.
        label_values

    Returns:
        Colour coded image for segmentation visualization
    """
    colour_codes = np.array(label_values)
    x = colour_codes[image.astype(int)]
    return x


# Perform reverse one-hot-encoding on labels / preds
def reverse_one_hot(image):
    """
    Transform a 2D array in one-hot format (depth is num_classes),
    to a 2D array with only 1 channel, where each pixel value is
    the classified class key.

    Args:
        image: The one-hot format image

    Returns:
        A 2D array with the same width and hieght as the input, but
        with a depth size of 1, where each pixel value is the classified
        class key.
    """
    x = np.argmax(image, axis=-1)
    return x


def seg_dataset(
    test_dataset, test_dataset_vis, DEVICE, best_model, select_class_rgb_values
):
    """Get the segmentation dataset."""
    for idx in range(len(test_dataset)):
        image, names = test_dataset[idx]
        # plant segmentation folder
        plant_folder = names.replace("_crop", "_seg").rsplit("/", 1)[0]
        if not os.path.exists(plant_folder):
            os.makedirs(plant_folder)
        # if np.mod(idx, 72) == 1:
        #     print(plant_folder)

        img_name = names.rsplit("/", 1)[-1]
        image_vis = test_dataset_vis[idx][0].astype("uint8")
        true_dimensions = image_vis.shape
        x_tensor = torch.from_numpy(image).to(DEVICE).unsqueeze(0)
        # Predict test image
        pred_mask = best_model(x_tensor)
        pred_mask = pred_mask.detach().squeeze().cpu().numpy()
        # Convert pred_mask from `CHW` format to `HWC` format
        pred_mask = np.transpose(pred_mask, (1, 2, 0))
        # Get prediction channel corresponding to foreground
        pred_mask = crop_image(
            colour_code_segmentation(
                reverse_one_hot(pred_mask), select_class_rgb_values
            ),
            true_dimensions,
        )["image"]
        pred_mask[np.all(pred_mask == [0, 0, 128], axis=-1)] = [255, 255, 255]
        cv2.imwrite(os.path.join(plant_folder, img_name), pred_mask)
    return plant_folder


def get_layer_boundary(image):
    img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gradient_y = cv2.Sobel(img_gray, cv2.CV_64F, 0, 1, ksize=5)
    gradient_avgy = np.mean(gradient_y, axis=1)

    top = 0  # the index from cropped location instead of original image
    start_filter_ind = 200
    end_filter_ind = -450
    ind = (
        np.argmax(gradient_avgy[start_filter_ind:end_filter_ind])
        + top
        + start_filter_ind
    )
    return ind


def get_layer_boundary_plant(crop_paths):
    boundary_idx_plant = pd.DataFrame()
    images = [
        os.path.join(path, file) for path in crop_paths for file in os.listdir(path)
    ]
    for image_path in images:
        frame = image_path.rsplit("/", 1)[-1]
        index_crop = image_path.find("_crop")
        plant = image_path[index_crop - 10 : index_crop]
        scan_ind_path = image_path[: index_crop - 11]
        image = cv2.imread(image_path)
        ind = get_layer_boundary(image)
        new_data = pd.DataFrame(
            [
                {
                    "scan_ind_path": scan_ind_path,
                    "plant": plant,
                    "frame": frame,
                    "layer_ind": ind,
                }
            ]
        )
        boundary_idx_plant = pd.concat(
            [boundary_idx_plant, new_data], ignore_index=True
        )
    return boundary_idx_plant


def get_root_area(layer_img):
    _, count = np.unique(layer_img[:, :, 0], return_counts=True)
    if len(count) > 1:
        root_area = count[1]
        total_area = count[0] + count[1]
    else:
        root_area = 0
        total_area = count[0]
    return root_area, total_area


def get_root_count(layer_count_img):
    contours, _ = cv2.findContours(
        layer_count_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
    )
    if len(contours) > 0:
        contour_areas = [cv2.contourArea(contour) for contour in contours]
        root_count = (
            len(contours)
            if np.max(contour_areas) < layer_count_img.size
            else len(contours) - 1
        )
    else:
        root_count = 0
    return root_count


def get_traits(layer_index_df):
    layer_boundary_threshold = 20
    root_count_depth = 5
    traits_df = pd.DataFrame()
    for scan_ind_path, plant, frame, layer_ind in zip(
        layer_index_df["scan_ind_path"],
        layer_index_df["plant"],
        layer_index_df["frame"],
        layer_index_df["layer_ind"],
    ):
        seg_path = os.path.join(scan_ind_path, plant + "_seg", frame)
        seg_image = cv2.imread(seg_path)

        # get the upper layer and bottom layer
        upper_layer = seg_image[: layer_ind - layer_boundary_threshold, :, :]
        bottom_layer = seg_image[layer_ind + layer_boundary_threshold :, :, :]

        # get area
        upper_root_area, upper_total_area = get_root_area(upper_layer)
        bottom_root_area, bottom_total_area = get_root_area(bottom_layer)
        area_ratio = np.divide(bottom_root_area, upper_root_area)

        # get root count
        upper_layer_count = upper_layer[-root_count_depth:, :, 0]
        bottom_layer_count = bottom_layer[:root_count_depth, :, 0]
        upper_root_count = get_root_count(upper_layer_count)
        bottom_root_count = get_root_count(bottom_layer_count)
        count_ratio = np.divide(bottom_root_count, upper_root_count)

        # concat to the df
        new_data = pd.DataFrame(
            [
                {
                    "scan_ind_path": scan_ind_path,
                    "plant": plant,
                    "frame": frame,
                    "layer_ind": layer_ind,
                    "upper_root_area": upper_root_area,
                    "upper_total_area": upper_total_area,
                    "bottom_root_area": bottom_root_area,
                    "bottom_total_area": bottom_total_area,
                    "upper_root_count": upper_root_count,
                    "bottom_root_count": bottom_root_count,
                    "area_ratio": area_ratio,
                    "count_ratio": count_ratio,
                }
            ]
        )
        traits_df = pd.concat([traits_df, new_data], ignore_index=True)
    return traits_df


def main(input_dir, output_dir):
    scans_csv = os.path.join(input_dir, "scans.csv")
    params_json = os.path.join(input_dir, "params.json")

    # load scans_csv as dataframe
    scans_df = pd.read_csv(scans_csv)

    # load params_json as dict
    with open(params_json) as f:
        params = json.load(f)
    model_name = params["model_name"]

    # add device
    DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # crop the original image in scans_df and save cropped images in {output}/crop
    crop_paths = crop_save_image_plant(scans_df)

    # write metadata
    metadata_path = write_metadata(crop_paths, input_dir)

    # load segmentation model and parameters
    (
        best_model,
        select_classes,
        select_class_rgb_values,
        preprocessing_fn,
    ) = setup_model_parameters(input_dir, model_name, DEVICE)

    # setup dataset
    test_dataset, test_dataset_vis = setup_dataset(
        metadata_path, select_class_rgb_values, preprocessing_fn
    )

    # predict cropped images
    seg_plant_folder = seg_dataset(
        test_dataset, test_dataset_vis, DEVICE, best_model, select_class_rgb_values
    )

    ## trait analysis
    # get layer index
    layer_index_df = get_layer_boundary_plant(crop_paths)

    # get traits based on the index and seg
    traits_df = get_traits(layer_index_df)
    traits_df.to_csv(os.path.join(output_dir, "traits_original.csv"), index=False)


if __name__ == "__main__":
    input_dir = sys.argv[1]
    output_dir = sys.argv[2]
    main(input_dir, output_dir)
