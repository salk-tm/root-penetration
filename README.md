# root-penetration


## Description
The `main` function ("segment.py") orchestrates the root penetration pipeline. It crops original imagess, segment the images, and compute the root penetration traits.


## Installation

**Make sure to have Docker Desktop running first**


First, pull the image if you don't have it built locally:

```
docker pull registry.gitlab.com/salk-tm/root-penetration:latest
```

Then, to run the image interactively:

```
docker run -it registry.gitlab.com/salk-tm/root-penetration:latest bash
```
To run the image interactively with data mounted to the container, use the syntax

```
docker run -v /path/on/host:/path/in/container [other options] image_name [command]

```

For example: 

```
docker run --gpus all -v C:/Work/gitlab/root-penetration/test:/home/test -it registry.gitlab.com/salk-tm/root-penetration:latest bash
```

To test the python file in test dir
```
python test.py ./data ./data/output
```



Then you can run the `main` function in the /home directory in the container using 
```
python data_cleanup/data_cleanup.py "data" "output_dir"
```
and check "/home/output_dir" for the expected files.

- The DockerFile installs the environment from `env.yaml`as the base environment on the container then copies "data_cleanup" to the home directory of the container. 
- The container should contain `/tmp/env.yaml` and `/home/data_cleanup`. 
- After navigating to the /home directory, the `main` function can be run inside the container using the syntax

`python data_cleanup/data_cleanup.py "<input_dir>" "<output_dir>"`


**Notes:**

- The `registry.gitlab.com` is the Docker registry where the images are pulled from. This is only used when pulling images from the cloud, and not necesary when building/running locally.
- `-it` ensures that you get an interactive terminal. The `i` stands for interactive, and `t` allocates a pseudo-TTY, which is what allows you to interact with the bash shell inside the container.
- The `-v` or `--volume` option mounts the specified directory with the same level of access as the directory has on the host.
- `bash` is the command that gets executed inside the container, which in this case is to start the bash shell.
- Order of operations is 1. Pull (if needed): Get a pre-built image from a registry. 2. Run: Start a container from an image.

## Support
contact Elizabeth at eberrigan@salk.edu

## Contributing
To work on and use this repo, 

1. Clone the repo: `git clone https://gitlab.com/salk-tm/data-cleanup.git`
2. Navigate inside the repo: `cd data-cleanup`
3. Build the environment: `mamba env create -f env.yaml`
4. Activate the environment: `mamba activate data-cleanup`

There is some test data in the `tests` directory. Examine the "tests/data" folder for an example `input_dir` and "tests/output_dir" for an example `output_dir`.
You can run the `main` function using the test data with the following command:

**make sure to change the slash direction depending on your OS**

```
python data_cleanup/data_cleanup.py "tests/data" "tests/output_dir"
```

- Please make a new branch, starting with your name, with any changes, and request a reviewer before merging with the main branch since this container will be used by all HPI.
- Please document using the same conventions (docstrings for each function and class, typing-hints, informative comments).
- Tests are written in the pytest framework. Data used in the tests are defined as fixtures in "tests/fixtures/data.py" ("https://docs.pytest.org/en/6.2.x/reference.html#fixtures-api").

## Build
To build via automated CI, just push to `main`. See [`.gitlab-ci.yml`](.gitlab-ci.yml) for the runner definition.

To build locally for testing:

```
docker build --platform linux/amd64 --tag registry.gitlab.com/salk-tm/data-cleanup:latest .
```
