import pandas as pd
import os

scans_csv = "./test/data/scans.csv"
scans_df = pd.read_csv(scans_csv)
output_dir = "./test/data/output"

for i in range(len(scans_df)):
    plant_name = scans_df["plant_qr_code"][i]
    scan_path = scans_df["scan_path"][i]
    # scanner = scans_df["scanner"][i]
    roi_x, roi_y = scans_df["roi_x"][i], scans_df["roi_y"][i]
    width, height = scans_df["roi_w"][i], scans_df["roi_h"][i]
    save_path = scan_path.replace("scans", os.path.join(output_dir, "crop"))
    print(
        f"plant_name: {plant_name} \nscan_path: {scan_path} \nsave_path: {save_path} \nroi_x: {roi_x} \nroi_y: {roi_y} \nroi_w: {width} \nroi_h: {height} \n"
    )
